# FIDELIO Chrome-Extension guide

                    FIDELIO Chrome-Extension guide
                    2017-11-03, krebs@adesso.de

## Introduction

There are two Plugins necessary because the Chrome-Browser doesn´t have whitelisted the FIDELIO-Plugin so the FIDO-Plugin is important
to whitelist the FIDELIO-Plugin and make it possible that the FIDELIO-Plugin can react on the related requests.<br/>
The FIDELIO-U2F-Extension for Google-Chrome emerging of the manifest.json consists of four scripts.<br/>
The fidelio.js, the background.js, the cbor.js and the jsonh.js.<br/>
The specific function-documentation for each of them is readable in the respective file.<br/>
The URLs for the FIDELIO-Server with the eService and for the eID-Client can be set in the fidelio.js.<br/>
The Chrome-Extension-Ids from the Plugins with whom the FIDELIO-Plugin cooperates are set in the background.js.

### Download

The FIDO-Plugin can be found at:<br/><br/>
https://github.com/google/u2f-ref-code/tree/master/u2f-chrome-extension

## Installation
						
After you have downloaded these plugin you have to edit the u2fbackground.js and insert or edit
the following code line after line number 72 to whitelist the FIDELIO plugin:<br/><br/>
<b>HELPER_WHITELIST.addAllowedExtension('mhaalmbiimaofkbfacibhpjojjnkjkpc');</b><br/><br/>
<b>'mhaalmbiimaofkbfacibhpjojjnkjkpc'</b> stands for the Plugin-ID of the FIDELIO-Plugin.

## Usage

To use FIDELIO on a Desktop-PC u need a Smartcard Reader connected, an installed and running eID-Client e.g. AusweisApp2
or PersoApp and as follows installed, activated Browser-Plugins:<br/>
1. open Google Chrome<br/>
2. go to Extensions:<br/>
2.1. click on the button with three horizontal dots on the upper right corner of the browser to open the main menu<br/>
2.2. scroll down to "More tools" (an extra menu will expand)<br/>
2.3. click on "Extensions" in this menu (a new tab with all the plugins installed will appear)<br/>
3. click on "Load unpacked extension..." and search for the location of the FIDELIO-and the FIDO-Plugin you´ve downloaded before<br/>
4. after you´ve loaded both extensions you should restart the Browser
