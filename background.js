/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

/**
 * @author: Markus Krebs, adesso AG, krebs@adesso.de
 * This file contains background operations for FIDELIO i.e. it handles the
 * Messaging between the FIDELIO and the Google-FIDO-Extension which is
 * necessary for the handling of User-Inputs
 */
/*
 * Message-handling between Plugins
 */
function messageHandlerExternal(request, sender, sendResponse) {
    messageHandler(request, sender, sendResponse);
    return true;
}

/*
 * Plugins which whom the FIDELIO-Extension builds an Message-channel, receives
 * and sends messages with the help of an MessageHandler the FIDELIO-Extension
 * listens for the Request the Google-FIDO-Plugin gets e.g. through User-Input
 */
chrome.runtime.onMessageExternal.addListener(messageHandlerExternal);
chrome.runtime.sendMessage('pfboblefjcgdjicmnffhdgionmgcdmne', chrome.runtime.id);
chrome.runtime.sendMessage('kmendfapggjehodndflmmgagdbamhnfd', chrome.runtime.id);

/*
 * give back an Console-Log with the specific Plugin-ID
 */
console.log("FIDELIO Plugin active as " + chrome.runtime.id);

/*
 * the Function that handles the Messaging-process
 */
function messageHandler(request, sender, sendResponse) {

    /*
     * check if an Enroll-Request is triggered
     */
    if (request.type === 'enroll_helper_request') {
        console.dir(request);

        var keyHandles = [];

        request.signData.forEach(function(signReq) {
            if (signReq.version === 'U2F_V2' && signReq.keyHandle.startsWith('8d4RA')) {
                if (!appIdHash || !clientDataHash) {
                    appIdHash = signReq.appIdHash;
                    clientDataHash = signReq.challengeHash;
                }
                keyHandles.push(signReq.keyHandle);
            }
        });
        request.enrollChallenges.forEach(function(enrollReq) {

            /*
             * if so handle every Challenge and check everyone if the Version of
             * the Request is equal to "U2F_V2"
             */
            if (enrollReq.version === 'U2F_V2') {

                /*
                 * if so make credential, appIdHash, clientDataHash, blackList,
                 * callback
                 */
                fidelioEN(enrollReq.appIdHash, enrollReq.challengeHash, keyHandles, function(response, credential) {
                    sendResponse({
                        "type" : "enroll_helper_reply",
                        "code" : 0, // from DeviceStatusCodes
                        "version" : "U2F_V2",
                        "enrollData" : response
                    });
                });
                return;
            } else {

                /*
                 * if not give back an Console-Log with the specific unsupported
                 * version
                 */
                console.log("Unsupported: " + enrollReq.version);
            }
        });
    } else if (request.type === 'sign_helper_request') {

        /*
         * check if a Signment-Request is triggered if so give back an
         * Console-Log with the request, handle every signData and check
         * everyone if the Version of the Signment-Request is equal to "U2F_V2"
         * and that the Keyhandle starts with 8d4RA
         */
        console.dir(request);

        /*
         * appIdHash should be the same for one web-application
         */
        var appIdHash;
        var clientDataHash;
        var keyHandles = [];
        request.signData.forEach(function(signReq) {
            if (signReq.version === 'U2F_V2' && signReq.keyHandle.startsWith('8d4RA')) {

                /*
                 * check if appIdHash or clientDataHash is set. if not set them
                 * to the Request specific values.
                 */
                if (!appIdHash || !clientDataHash) {
                    appIdHash = signReq.appIdHash;
                    clientDataHash = signReq.challengeHash;
                }
                keyHandles.push(signReq.keyHandle);
            }
        });

        /*
         * check if keys exist get assertion, appIdHash, clientDataHash,
         * whiteList, callback
         */
        if (keyHandles && keyHandles.length > 0) {
            fidelioAU(appIdHash, clientDataHash, keyHandles, function(response, credential) {
                sendResponse({
                    "type" : "sign_helper_reply",
                    "code" : 0, // from DeviceStatusCodes
                    "errorDetail" : undefined,
                    "responseData" : {
                        "version" : "U2F_V2",
                        "appIdHash" : appIdHash,
                        "challengeHash" : clientDataHash,
                        "keyHandle" : credential,
                        "signatureData" : response
                    }
                });
            });
        } else { // No Keys found case
            console.log("No keys.");
            sendResponse({
                "type" : "sign_helper_reply",
                "code" : -1,// from DeviceStatusCodes
                "errorDetail" : "No key",
                "responseData" : {}
            });
        }

        /*
         * check if a Close-Request is triggered
         */
    } else if (request.type === 'close') {
        console.log("Got close notify");
    } else {
        console.log("Unknown request: " + request.type);
    }
}
